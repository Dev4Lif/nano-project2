## ND9991 - C2- Infrastructure as Code - Supporting Material and Starter Code
This folder provides the supporting material and starter code for the "ND9991 - C2- Infrastructure as Code" course. This folder contains the following folders:
1. project_starter - It contains the starter code.
2. project_final - It contains the updated essential files (.yml, .json, .bat, .sh, and .jpeg) for this exercise.

### Dependencies
##### 1. AWS account
You would require to have an AWS account to be able to build cloud infrastructure.

##### 2. VS code editor
An editor would be helpful to visualize the image as well as code. Download the VS Code editor [here](https://code.visualstudio.com/download).

##### 3. An account on www.lucidchart.com
A free user-account on [www.lucidchart.com](www.lucidchart.com) is required to be able to draw the web app architecture diagrams for AWS.


### How to run the scripts?
You can run the supporting material in two easy steps:
```bash
# Ensure that the AWS CLI is configured before runniing the command below
# Create the network infrastructure
create-network.bat
# Update the network infrastructure (if needed)
update-network.bat
# Check & Change the AMI ID and key-pair name in the servers.yml if required depending on target availability zone (AZ)
# Create the servers infrastructure
create-servers.bat
# Update the servers infrastructure (if needed)
update-servers.bat
```

### S3 Bucket Role & Policy
Project2-S3-Role and Project2-S3-Policy are to be created.
```bash
{
    "Version": "2012-10-17",
    "Statement": [
        {
        "Action": ["s3:ListAllMyBuckets", "s3:GetBucketLocation"],
        "Effect": "Allow",
        "Resource": ["arn:aws:s3:::*"]
        },
        {
            "Effect": "Allow",
            "Action": [
                "s3:Get*",
                "s3:List*"
            ],
            "Resource": [
                "arn:aws:s3:::project2-public-bucket/*",
                "arn:aws:s3:::project2-public-bucket"
            ]
        }
    ]
}
```

### How to use Bastion Server?
> Create Project2KeyPair via EC2 AWS Console and download the .pem file to local machine

- SSH into Bastion Server using Connect in EC2 Instance
```bash
sudo su (To change to root user with all admin rights)
vi Project2KeyPair.pem
Type the letter - i (To go into insert mode in shell terminal)
Copy-Paste contenst of Project2KeyPair
Press ESC
Type :wq and hit enter (To save the file Project2KeyPair.pem)
chmod 400 Project2KeyPair.pem (To avoid pem contents to be publicly viewable)
```

- To SSH Into problematic Ubuntu EC2 Instance (Get Private IP from EC2 Instances)
```bash 
ssh -i "Project2KeyPair.pem" ubuntu@10.0.3.80
```

- S3 Scripts
```bash 
sudo su
mkdir /var/www/temp
cd /var/www/temp/
curl https://project2-public-bucket.s3.amazonaws.com/udagram.zip -o /var/www/temp/udagram.zip (Alternate 1)
aws s3 cp s3://project2-public-bucket/udagram.zip /var/www/temp/udagram.zip (Alternate 2)
unzip -d temp-content udagram.zip 
cp -r /var/www/temp/temp-content/udagram/* /var/www/html
rm -rf temp-content udagram.zip
```

- Install AWS CLI in EC2 Instance (If Needed)
As a pre-requisite, you should first ssh into the target EC2 Instance in Private Subnet.
```bash
curl "https://awscli.amazonaws.com/awscli-exe-linux-x86_64.zip" -o "awscliv2.zip"
unzip awscliv2.zip
sudo ./aws/install
rm -rf aws awscliv2.zip
```

### Tutorials
https://www.youtube.com/watch?v=iwSSpN5l1zk - Bastion Host
https://grapeup.com/blog/automating-your-enterprise-infrastructure-part-2-cloud-infrastructure-as-code/ - Infrastructure As Code
